# xas-simulator

[Nexpy](https://github.com/nexpy/nexpy) plugin to simulate XAS spectra

It uses [Quanty](https://www.quanty.org/) to do multiplet calculations under different approximations.
